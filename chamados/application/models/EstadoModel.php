<?php

class EstadoModel extends CI_Model {

    private $_estado;

    public function __construct(IEstado $estado = null) {
        // Call the CI_Model constructor
        parent::__construct();

        $this->_estado = $estado;
    }

    /**
     * Retorna array com todos os estados
     * @return Array
     */
    public function getAllEstados() {
        $query = $this->db->get($this->_estado->getTable());
        return $query->custom_result_object(get_class($this->_estado));
    }

    /**
     * Retorna o estado de acordo com id selecionado
     * @return Array
     */
    public function getEstadoById($id) {
        $this->db->where('id_estado', $id);
        $query = $this->db->get($this->_estado->getTable());
        return $query->row_array();
    }

    /**
     * Prepara Array para montar combobox
     * @param Array(Estados) $estados
     * @return type
     */
    public function montaListaCombobox($estados) {
        $arr_retorno = array("" => "Selecione");
        foreach ($estados as $r) {
            $arr_retorno[$r->id_estado] = $r->nome;
        }

        return $arr_retorno;
    }

}
