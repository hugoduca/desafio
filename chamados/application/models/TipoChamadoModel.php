<?php

class TipoChamadoModel extends CI_Model {

    private $_tipoChamado;

    public function __construct(ITipoChamado $tipo = null) {
        // Call the CI_Model constructor
        parent::__construct();

        $this->_tipoChamado = $tipo;
    }

    /**
     * Retorna lista com todos os tipos
     * @return Array
     */
    public function getAllTipos() {
        $query = $this->db->get($this->_tipoChamado->getTable());
        return $query->custom_result_object(get_class($this->_tipoChamado));
    }

    /**
     * Retorna o tipo de acordo com id selecionado
     * @return Array
     */
    public function getTipoById($id) {
        $this->db->where('id_tipo_chamado', $id);
        $query = $this->db->get($this->_tipoChamado->getTable())->row_array();
        return $query;
    }

    /**
     * Prepara Array para montar combobox
     * @param Array(TipoChamado) $tipo
     * @return array
     */
    public function montaListaCombobox($tipo) {
        $arr_retorno = array("" => "Selecione");
        foreach ($tipo as $r) {
            $arr_retorno[$r->id_tipo_chamado] = $r->tipo_chamado;
        }

        return $arr_retorno;
    }

}
