<?php
include APPPATH.'interfaces/ITipoChamado.php'; 

class TipoChamado implements ITipoChamado{
    public $id_tipo_chamado;
    public $tipo_chamado;
    
    /**
     * Retorna o nome da tabela onde serão persistidos os dados desse objeto
     * @return string
     */
     public function getTable() {
        return "tipo_chamado";
    }
}
