<?php

include APPPATH . 'interfaces/IChamado.php';

class Chamado implements IChamado {

    public $id_chamado;
    public $descricao;
    public $id_estado;
    public $id_tipo_chamado;
    public $id_motivo_chamado;
    public $data_encerramento;
    private $error_message;

    
    public function __construct() {
        $this->setDataEncerramento();
    }
    /**
     * Verifica se objeto é um chamado Válido
     * @return Boolean true para valido, false para inválido
     */
    public function is_valid() {
        $this->error_message = "";
		$descricao = trim($this->descricao);   
        if (empty($descricao)) {
            $this->error_message = "Informe o cammpo Descrição. <br>";
            return false;
        }
        if (!is_numeric($this->id_estado)) {            
            $this->error_message = "Selecione o estado. <br>";
            return false;
        }
        if (!is_numeric($this->id_tipo_chamado)) {
            $this->error_message = "Selecione o Tipo. <br>";
            return false;
        }
        if (!is_numeric($this->id_motivo_chamado)) {
            $this->error_message = "Selecione o motivo. <br>";
            return false;
        }
        if (empty($this->data_encerramento) or !ValidDateBD($this->data_encerramento)) {
            $this->error_message = "Data de Encerramento Inválida <br>";
            return false;
        }
        if (($this->data_encerramento) != (date('Y-m-d'))) {
            $this->error_message = "Data de Encerramento diferente do dia de hoje <br>";
           
            return false; //data nao pode ser maior nem menor que a data atual
        }
        return true;
    }

    public function setDataEncerramento($data = null) {
   
        if ( is_null($data)) {
            $this->data_encerramento = date('Y-m-d');
        } else {
            $this->data_encerramento = $data;
        }
       // var_dump($this->data_encerramento );
    }

    /**
     * Retorna o nome da tabela onde serão persistidos os dados desse objeto
     * @return string
     */
    public function getTable() {
        return "chamado";
    }

    /**
     * Exibe mensagem de erro gerada pelo metodo id_valid
     * @return String
     */
    public function getErrorMsg(){
        return $this->error_message;
    }
}
