<?php

include APPPATH.'interfaces/IEstado.php'; 

class Estado implements IEstado {
    public $id_estado;
    public $uf;
    public $nome;
    
    /**
     * Retorna o nome da tabela onde serão persistidos os dados desse objeto
     * @return string
     */
    public function getTable() {
        return "estado";
    }
    
   
}
