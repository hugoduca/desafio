<?php
include APPPATH.'interfaces/IMotivoChamado.php'; 

class MotivoChamado implements IMotivoChamado {
    public $id_motivo_chamado;
    public $motivo;
    
    /**
     * Retorna o nome da tabela onde serão persistidos os dados desse objeto
     * @return string
     */
     public function getTable() {
        return "motivo_chamado";
    }
}
