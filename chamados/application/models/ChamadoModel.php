<?php

class ChamadoModel extends CI_Model {

    private $chamado;

    public function __construct(IChamado $chamado = null) {
        parent::__construct();

        $this->chamado = $chamado;
    }

    /**
     * retorna todos os atendimentos registrados, agrupando por dia do atendimento, estado e ordenados pela data de atendimento, do mais atual para o mais antigo
     * @return Array array de chamados
     */
    public function listAllChamados() {
       
        $this->db->order_by('data_encerramento', 'desc');
        $this->db->order_by( $this->chamado->getTable() . '.id_estado');
        $this->db->join('estado', 'estado.id_estado = ' . $this->chamado->getTable() . '.id_estado');
        $this->db->join('motivo_chamado', 'motivo_chamado.id_motivo_chamado = ' . $this->chamado->getTable() . '.id_motivo_chamado');
        $this->db->join('tipo_chamado', 'tipo_chamado.id_tipo_chamado = ' . $this->chamado->getTable() . '.id_tipo_chamado');
        $query = $this->db->get($this->chamado->getTable());
       
        return $query->result();
    }

    /**
     * Persiste os dados no banco de dados
     * @param Chamado $chamado
     * @return boolean
     */
    public function cadastrar(Chamado $chamado) {
        return $this->db->insert($this->chamado->getTable(), $chamado);
    }
    
    /**
     * Obtem os dados de um chamado
     * @param int $id
     * @return Chamado
     */
    public function getChamadoById($id) {
        $this->db->where('id_chamado', $id);
        $query = $this->db->get($this->chamado->getTable())->row_array();
        
        return $query;
    }

}
