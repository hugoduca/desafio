<?php

class MotivoChamadoModel extends CI_Model {

    private $_motivo;
    
       public function __construct(IMotivoChamado $motivo=null) {
        // Call the CI_Model constructor
        parent::__construct();
        
        $this->_motivo = $motivo;
    }
 

    /**
     * Retorna lista com todos os motivos
     * @return Array
     */
    public function getAllMotivos() {
        $query = $this->db->get($this->_motivo->getTable());        
        return $query->custom_result_object(get_class($this->_motivo));
    }

    /**
     * Retorna o motivo de acordo com id selecionado
     * @return Array
     */
    public function getMotivoById($id) {
        $this->db->where('id_motivo_chamado', $id);
        $query = $this->db->get($this->_motivo->getTable());        
        return $query->row_array();
    }
    
    
     /**
     * Prepara Array para montar combobox
     * @param Array(MotivoChamado) $motivo
     * @return array
     */
    public function montaListaCombobox($motivo) {
        $arr_retorno = array(""=>"Selecione");
        foreach ( $motivo as  $r) {
            $arr_retorno[$r->id_motivo_chamado] = $r->motivo;
        }

        return $arr_retorno;
    }

}
