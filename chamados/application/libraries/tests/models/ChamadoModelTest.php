<?php

/**
 * Classe responsável pelos testes unitários da classe /models/chamadoModel
 *
 * @author Hugo
 */
class ChamadoModelTest {

    private $_objChamadoModel;
    public $tester;
    public $ci;

    public function __construct(ChamadoModel $chamadoModel = null) {
        $this->ci = get_instance();
        $this->ci->load->library('unit_test');
        $this->_objChamadoModel = $chamadoModel;
    }

    /**
     * @return Void imprime na tela resultadodo teste unitario
     */
    public function testBuscandoChamadoUsandoIdInvalido() {

        $teste = $this->_objChamadoModel->getChamadoById("literal");
        $valorEsperado = null;
        $teste_name = "Passando string no lugar de id. ";
        $notes = " Classe (" . get_class($this->_objChamadoModel) . ") Método: getChamadoById()";
        echo $this->ci->unit->run($teste, $valorEsperado, $teste_name, $notes);
    }

    /**
     * @return Void imprime na tela resultadodo teste unitario
     */
    public function testCadastroDeChamadoComDadosInvalidos() {

        $chamado = $this->getChamadoStubInvalido();

        $teste = $this->_objChamadoModel->cadastrar($chamado);
        $valorEsperado = false;
        $teste_name = "Tentando cadastrar um objeto que não é valido. ";
        $notes = " Classe (" . get_class($this->_objChamadoModel) . ") Método: cadastrar()";
        echo $this->ci->unit->run($teste, $valorEsperado, $teste_name, $notes);
    }

    /**
     * @return Void imprime na tela resultadodo teste unitario
     */
    public function testCadastroDeChamadoComDadosValidos() {

        $this->ci->db->trans_start(); //abrindo transação

        $chamado = $this->getChamadoStubValido();

        $teste = $this->_objChamadoModel->cadastrar($chamado);
        $valorEsperado = true;
        $teste_name = "Tentando cadastrar um objeto  valido. ";
        $notes = " Classe (" . get_class($this->_objChamadoModel) . ") Método: cadastrar()";
        echo $this->ci->unit->run($teste, $valorEsperado, $teste_name, $notes);

        $this->ci->db->trans_rollback(); //desfazendo alterações
    }

    /**
     * @return Void imprime na tela resultadodo teste unitario
     */
    public function testDeIntegracaoCadastroEConsultaDeChamado() {

        $this->ci->db->trans_start(); //abrindo transação


        $chamado = $this->getChamadoStubValido();
        $this->_objChamadoModel->cadastrar($chamado);

        $cadastrado = $this->_objChamadoModel->getChamadoById($this->ci->db->insert_id());
        $teste = $cadastrado['id_motivo_chamado'] == $chamado->id_motivo_chamado && $cadastrado['id_tipo_chamado'] = $chamado->id_tipo_chamado  ;
        $valorEsperado = true;
        $teste_name = "Resgatando um Objeto Após realizar o seu cadastro";
        $notes = "Teste de Integração, Classe (" . get_class($this->_objChamadoModel) . ") Método: cadastrar() e getChamadoById()";
        echo $this->ci->unit->run($teste, $valorEsperado, $teste_name, $notes);

        $this->ci->db->trans_rollback(); //desfazendo alterações
    }

    /**
     * retorna um Chamado Válido
     * @return Chamado objeto chamado populado
     */
    private function getChamadoStubValido() {
        $chamado = new Chamado();
        $chamado->descricao = "Quem é você";
        $chamado->id_motivo_chamado = "3";
        $chamado->id_tipo_chamado = "1";
        $chamado->id_estado = 7;
        return $chamado;
    }

    /**
     * retorna um Chamado InVálido
     * @return Chamado objeto com dados inválidos
     */
    private function getChamadoStubInvalido() {
        $chamado = new Chamado();
        return $chamado;
    }

}
