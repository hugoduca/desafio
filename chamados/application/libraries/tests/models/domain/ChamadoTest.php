<?php

/**
 * Classe responsável pelos testes unitários da classe /models/domain/chamado
 *
 * @author Hugo
 */
class ChamadoTest {

    private $_objChamado;
    public $tester;
    public $ci;

    public function __construct(Chamado $chamado = null) {
        $this->ci = get_instance();
        $this->ci->load->library('unit_test');

        $this->_objChamado = $chamado;
    }

    /**
     * @return Void imprime na tela resultadodo teste unitario
     */
    public function testVerificaSeRetornoDoMetodoEBoolean() {

        $teste = $this->_objChamado->is_valid();

        $teste_name = "Verificando retorno booleano. ";
        $notes = " Classe (" . get_class($this->_objChamado) . ") Método: is_valid()";
        return $this->ci->unit->run($teste, 'is_bool', $teste_name, $notes);
    }

    /**
     * @return Void imprime na tela resultadodo teste unitario
     */
    public function testDataEncerramentoInvalida() {
        //POPULANDO objeto  de testes
        $this->setObjStubChamado(1, 1, 1, "Descricao", date('Y-m-d', strtotime(' + 5 days')));
        $teste = $this->_objChamado->is_valid();
        $teste_name = "Verificando data de encerramento maior do que hoje. ";
        $valorEsperado = false;
        $notes = " Classe (" . get_class($this->_objChamado) . ") Método: is_valid()";
        echo $this->ci->unit->run($teste, $valorEsperado, $teste_name, $notes);

        $this->_objChamado->setDataEncerramento(date('Y-m-d', strtotime(' - 5 days')));
        $teste = $this->_objChamado->is_valid();
        $teste_name = "Verificando data de encerramento menor do que hoje. ";
        $valorEsperado = false;
        $notes = " Classe (" . get_class($this->_objChamado) . ") Método: is_valid()";
        echo $this->ci->unit->run($teste, $valorEsperado, $teste_name, $notes);

        $this->_objChamado->setDataEncerramento(date("Y-m-d"));
        $teste = $this->_objChamado->is_valid();
        $teste_name = "Verificando data de encerramento com data de hoje. ";
        $valorEsperado = true;
        $notes = " Classe (" . get_class($this->_objChamado) . ") Método: is_valid()";
        echo $this->ci->unit->run($teste, $valorEsperado, $teste_name, $notes);

        $this->_objChamado->setDataEncerramento(1123123);
        $teste = $this->_objChamado->is_valid();
        $teste_name = "Verificando data de encerramento com valor de data Inválido. ";
        $valorEsperado = false;
        $notes = " Classe (" . get_class($this->_objChamado) . ") Método: is_valid()";
        echo $this->ci->unit->run($teste, $valorEsperado, $teste_name, $notes);
        $this->clearObjChamado();
    }

    /**
     * @return Void imprime na tela resultadodo teste unitario
     */
    public function testCamposEmBranco() {
        $this->setObjStubChamado(1, '', 1, " ", date('Y-m-d'));

        $teste = $this->_objChamado->is_valid();
        $teste_name = "Verificando campos preenchido com espaço em branco. ";
        $valorEsperado = false;
        $notes = " Classe (" . get_class($this->_objChamado) . ") Método: is_valid()";
        echo $this->ci->unit->run($teste, $valorEsperado, $teste_name, $notes);
        $this->clearObjChamado();
    }

    private function clearObjChamado() {
        $this->_objChamado->data_encerramento = date("Y-m-d");
        $this->_objChamado->id_estado = 1;
        $this->_objChamado->id_motivo_chamado = 1;
        $this->_objChamado->id_tipo_chamado = 1;
        $this->_objChamado->descricao = 'descricao';
    }

    /**
     * Popula objeto de Chamado Para teste
     * @param type $id_estado
     * @param type $id_motivo_chamado
     * @param type $id_tipo_chamado
     * @param type $descricao
     * @param type $data_encerramento
     * @return Void popula atributo _objChamado da classe
     */
    private function setObjStubChamado($id_estado, $id_motivo_chamado, $id_tipo_chamado, $descricao, $data_encerramento) {
        $this->_objChamado->data_encerramento = $data_encerramento;
        $this->_objChamado->id_estado = $id_estado;
        $this->_objChamado->id_motivo_chamado = $id_motivo_chamado;
        $this->_objChamado->id_tipo_chamado = $id_tipo_chamado;
        $this->_objChamado->descricao = $descricao;
    }

}
