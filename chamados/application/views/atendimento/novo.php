<script src="<?php echo base_url('/assets/js/scripts/atendimento/novo.js') ?>" ></script>
<div style="clear: both">&nbsp;</div>
<div class="container theme-showcase" role="main">

    <div class="page-header">
        <h1><?php echo $titulo; ?></h1>
    </div>
    <div class="row">
        <div class="col-md-10 ">
            <form id="frmNovo" role="form" method="post" action="<?php echo site_url('/Atendimento/registrarAtendimento'); ?>">
                <div class="form-group">
                    <label for="tipo_chamado">Tipo de Chamado:</label>
                    <?php echo form_dropdown('id_tipo_chamado', $tipos, '', 'class="form-control" required'); ?>
                </div>
                <div class="form-group">
                    <label for="id_estado">Estado:</label>
                    <?php echo form_dropdown('id_estado', $estados, '', 'class="form-control" required'); ?>
                </div>
                <div class="form-group">
                    <label for="id_motivo">Motivo:</label>
                    <?php echo form_dropdown('id_motivo', $motivos, '', 'class="form-control" required'); ?>
                </div>
                <div class="form-group">
                    <label for="pwd">Detalhes:</label>
                    <textarea class="form-control" id="descricao" name="descricao" required></textarea>
                </div>

                <button type="submit" class="btn btn-default">Cadastrar</button>
            </form>
        </div>
    </div>
    <div style="clear: both">&nbsp;</div>
    <div class="row">
        <div class="col-md-10 ">
            <div class="alert alert-success" role="alert" id="sucesso" style="display: none">
                <strong>Sucesso!</strong> <span></span>
            </div>
            <div class="alert alert-danger" role="alert" id="falha" style="display: none">
                <strong>Atenção!</strong> <span></span>
            </div>
        </div>
    </div>
