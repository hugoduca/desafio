<script src="<?php echo base_url('/assets/js/scripts/home/listagem.js') ?>" ></script>
<div style="clear: both">&nbsp;</div>
<div class="container theme-showcase" role="main">

    <div class="page-header">
        <h1>Listagem de Atendimentos</h1>
    </div>
    <div class="row">
        <div class="col-md-10 ">
            <a href="<?php echo site_url('/Atendimento/novo'); ?>" class="btn btn-sm btn-primary" type="button">Novo Atendimento</a>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Dt. Atendimento</th>
                        <th>Estado</th>
                        <th>Tipo</th>
                        <th>Motivo</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($lista as $chamado) {

                        echo "<tr>"
                        . "<td>" . $chamado->id_chamado . "</td>"
                        . "<td>" . formatDateBR($chamado->data_encerramento) . "</td>"
                        . "<td>" . $chamado->nome . "</td>"
                        . "<td>" . $chamado->tipo_chamado . "</td>"
                        . "<td>" . $chamado->motivo . "</td>"
                        . "<td><a href='#' onclick='loadData(". $chamado->id_chamado .")' class='btn btn-sm btn-link' type='button' data-toggle='modal' data-target='#myModal'>Visualizar</a></td>"
                        . "</tr>";
                    }
                    ?>                  
                </tbody>
            </table>
        </div>
    </div>
    
    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>

  </div>
</div>
</div>

