<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    private $chamadoModel;
    private $estadoModel;
    private $tipoChamadoModel;
    private $motivoChamadoModel;

    public function __construct() {
        parent::__construct();
        
        $chamado = new Chamado();
        $this->chamadoModel = new ChamadoModel($chamado);

        $estado = new Estado();
        $this->estadoModel = new EstadoModel($estado);
        
        $tipo = new TipoChamado();
        $this->tipoChamadoModel = new TipoChamadoModel($tipo);
        
        $motivo = new MotivoChamado();
        $this->motivoChamadoModel = new MotivoChamadoModel($motivo);
    }

    public function index() {

        $dados['lista'] = $this->chamadoModel->listAllChamados();
        $this->load->view('template/header');
        $this->load->view('home/listagem', $dados);
        $this->load->view('template/footer');
    }    
}
