<?php

class Test extends CI_Controller {

    public function index() {
        $this->DomainChamadoTests();
        $this->chamadoModelTests();
    }

    /**
     * Método responsável pelo call de todos os testes unitarios referentes ao Domain chamados
     * @return String report do teste unitario
     */
    public function DomainChamadoTests() {
        $this->load->library('tests/models/domain/ChamadoTest');
        
        $chamado = new Chamado();
        $chamadoTestes = new ChamadoTest($chamado);

         $chamadoTestes->testCamposEmBranco();       

         $chamadoTestes->testDataEncerramentoInvalida();

         $chamadoTestes->testVerificaSeRetornoDoMetodoEBoolean();
        
    }

     public function chamadoModelTests() {
        $this->load->library('tests/models/ChamadoModelTest');
        
        $chamado = new Chamado();
        $chamadoModel = new ChamadoModel($chamado);
        $chamadoModelTestes = new ChamadoModelTest($chamadoModel);

        $chamadoModelTestes->testBuscandoChamadoUsandoIdInvalido();
        $chamadoModelTestes->testCadastroDeChamadoComDadosInvalidos();
        $chamadoModelTestes->testCadastroDeChamadoComDadosValidos();
        $chamadoModelTestes->testDeIntegracaoCadastroEConsultaDeChamado();
        
        //echo $this->unit->report();
    }

}
