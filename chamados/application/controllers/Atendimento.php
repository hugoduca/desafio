<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Atendimento extends CI_Controller {

    private $chamadoModel;
    private $estadoModel;
    private $tipoChamadoModel;
    private $motivoChamadoModel;

    public function __construct() {
        parent::__construct();
        
        $chamado = new Chamado();
        $this->chamadoModel = new ChamadoModel($chamado);

        $estado = new Estado();
        $this->estadoModel = new EstadoModel($estado);

        $tipo = new TipoChamado();
        $this->tipoChamadoModel = new TipoChamadoModel($tipo);

        $motivo = new MotivoChamado();
        $this->motivoChamadoModel = new MotivoChamadoModel($motivo);
    }

    public function novo() {

        $data['titulo'] = "Novo Atendimento";
        $data['estados'] = $this->estadoModel->montaListaCombobox($this->estadoModel->getAllEstados());
        $data['tipos'] = $this->tipoChamadoModel->montaListaCombobox($this->tipoChamadoModel->getAllTipos());
        $data['motivos'] = $this->motivoChamadoModel->montaListaCombobox($this->motivoChamadoModel->getAllMotivos());

        $this->load->view('template/header');
        $this->load->view('atendimento/novo', $data);
        $this->load->view('template/footer');
    }

    /**
     * Realiza a ação de cadastro do atendimento no BD
     * @param Chamado http post
     * @return string
     */
    public function registrarAtendimento() {

        $retorno = "";
        $sucesso = false;
        $chamado = new Chamado();

        //resgatando os dados enviados via http post
        $chamado->id_estado = $this->input->post('id_estado');
        $chamado->id_motivo_chamado = $this->input->post('id_motivo');
        $chamado->id_tipo_chamado = $this->input->post('id_tipo_chamado');
        $chamado->descricao = $this->input->post('descricao');

        if ($chamado->is_valid()) {
            //cadastrar
            $sucesso = $this->chamadoModel->cadastrar($chamado);
        } else {
            //retorna erro            
            $retorno = json_encode(array("error" => "true", "msg" => $chamado->getErrorMsg()));
        }

        if ($sucesso) {
            $retorno = json_encode(array("error" => false, "msg" => "Chamado Registrado com sucesso"));
        }

        echo $retorno;
    }

    /**
     * Resgata um atendimento e retorna Json com os dados para consulta
     * @return String json contendo os dados do chamado
     */
    public function ConsultarAtendimento() {
        $id = $this->input->get('id');
        $chamados = $this->chamadoModel->getChamadoById($id);
		$chamados['data_encerramento']= formatDateBR($chamados['data_encerramento']);
        $chamados['estado'] = $this->estadoModel->getEstadoById($chamados['id_estado'])['nome'];
        $chamados['tipo'] = $this->tipoChamadoModel->getTipoById($chamados['id_tipo_chamado'])['tipo_chamado'];
        $chamados['motivo'] = $this->motivoChamadoModel->getMotivoById($chamados['id_motivo_chamado'])['motivo'];
        echo json_encode($chamados);
    }

}
