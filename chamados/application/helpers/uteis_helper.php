<?php

if (!function_exists('ValidDateBD')) {

    function ValidDateBD($dat) {
        $data = explode("-", "$dat"); // fatia a string $dat em pedados, usando / como referência
        $d = @$data[2];
        $m = @$data[1];
        $y = @$data[0];

        // verifica se a data é válida!
        // 1 = true (válida)
        // 0 = false (inválida)
        $res = checkdate($m, $d, $y);
        return $res;
    }

}

if (!function_exists('formatDateBR')) {

    function formatDateBR($dat) {
        $data = explode("-", "$dat"); // fatia a string $dat em pedados, usando / como referência
        $d = @$data[2];
        $m = @$data[1];
        $y = @$data[0];

        // verifica se a data é válida!
        // 1 = true (válida)
        // 0 = false (inválida)
        $res = $d."/".$m."/".$y;
        return $res;
    }

}