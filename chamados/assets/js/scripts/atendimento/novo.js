$(document).ready(function () {

    var options = {
        success: processJson, // executa metodo processJson após o submit
        beforeSubmit: limpaCampos, //executa metodo antes de realizar o submit
        dataType: 'json'

    };

    // bind form using 'ajaxForm' 
    $('#frmNovo').ajaxForm(options);
});

function processJson(data) {

    if (data.error) {
        $('#falha').show();
        $('#falha span').html(data.msg);
    } else {
        $('#sucesso').show().delay(6000).fadeOut();;
        $('#sucesso span').html(data.msg);
        $('#frmNovo').resetForm();
    }

}

function limpaCampos() {
    $('#falha').hide();
    $('#sucesso').hide();
}