function loadData(id) {
    $.ajax({
        url: "Atendimento/ConsultarAtendimento",
        context: document.body,
        dataType: 'json',
        data: {id: id},
        beforeSend: limparModal
    })

            .done(function (data) {

                $('.modal-title').html("<b>Atendimento n° " + data.id_chamado + "</b>");
                $('.modal-body').append("<p>Data do atendimento: " + data.data_encerramento + "</a>");
                $('.modal-body').append("<p>Estado: " + data.estado + "</a>");
                $('.modal-body').append("<p>Tipo: " + data.tipo + "</a>");
                $('.modal-body').append("<p>Motivo: " + data.motivo + "</a>");
                $('.modal-body').append("<p>Descrição:<br> " + data.descricao + "</a>");
            });
}

function limparModal() {
    $('.modal-body').html("");
    $('.modal-title').html("");
}