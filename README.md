# SAC - CHAMADOS#

Esse projeto foi desenvolvido como parte de processo seletivo para oportunidade de desenvolvedor.

### Informações sobre o projeto ###

* Sistema para registro de atendimentos de um SAC, nesta versão inclui:

1. Módulo de listagem de atendimentos, ordenados por data e agrupados por estado;
2. Módulo de registro de atendimento.


### Como instalar/configurar ###

1. CONFIGURAÇÃO

* Após realizar download do código fonte, extraia todo conteudo em seu diretório web PHP.

* Acesse o arquivo localizado em /chamados/application/config/**config.php** e altere a variavel **$config['base_url']**, coloque o domínio de onde será executada a aplicação, exemplo: 

```
#!php

$config['base_url'] = 'http://localhost/chamados';
```

* Acesse o arquivo localizado em /chamados/config/**database.php**, localize a configuração do banco de dados, e altere hostname, username e password de acordo com as configurações de seu banco de dados.

```
#!php

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'root',
	'password' => '',
```


.** DEPENDÊNCIAS**

1.  PHP versão 5.4 ou superior
2.  Mysql versão 5 ou superior
3.  Jquery
4.  Bootstrap

* **INSTALAÇÃO DAS DEPENDÊNCIAS**

1.  Para fazer download das dependências (**Jquery** e **Bootstrap**), será utilize o comando **bower install**, dentro da pasta **/chamados.** Para saber como utilizar o Bower, acesse o link https://bower.io/

* **CRIAÇÃO DO DATABASE**

1.  Execute o arquivo /install/**script_database.sql** em seu banco de dados MySql.

2.  Apague a pasta install
 
* **COMO ACESSAR O SISTEMA?**

1.  Acesse pelo navegador: http://<<seudominio>>/chamados

2.  caso seu dominio seja localhost: http://localhost/chamados/

* COMO EXECUTAR TESTES AUTOMATIZADOS ?

1.  Acesse pelo navegador: http://<<seudominio>>/chamadosindex.php/Test

2.  caso seu domínio seja localhost: http://localhost/chamados/index.php/Test


### CONTATO ###

* hugoduca@gmail.com