create schema chamadosdb;

USE chamadosdb;

CREATE TABLE `estado` (
  `id_estado` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador do estado',
  `uf` varchar(2) NOT NULL COMMENT 'sigla do estado',
  `nome` varchar(50) NOT NULL COMMENT 'nome do estado',
  PRIMARY KEY (`id_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;


insert into `estado`(`id_estado`,`uf`,`nome`) values (2,'AL','Alagoas');
insert into `estado`(`id_estado`,`uf`,`nome`) values (3,'AP','Amap�');
insert into `estado`(`id_estado`,`uf`,`nome`) values (4,'AM','Amazonas');
insert into `estado`(`id_estado`,`uf`,`nome`) values (5,'BA','Bahia');
insert into `estado`(`id_estado`,`uf`,`nome`) values (6,'CE','Cear�');
insert into `estado`(`id_estado`,`uf`,`nome`) values (7,'DF','Distrito Federal');
insert into `estado`(`id_estado`,`uf`,`nome`) values (8,'ES','Esp�rito Santo');
insert into `estado`(`id_estado`,`uf`,`nome`) values (9,'GO','Goi�s');
insert into `estado`(`id_estado`,`uf`,`nome`) values (10,'MA','Maranh�o');
insert into `estado`(`id_estado`,`uf`,`nome`) values (11,'MT','Mato Grosso');
insert into `estado`(`id_estado`,`uf`,`nome`) values (12,'MS','Mato Grosso do Sul');
insert into `estado`(`id_estado`,`uf`,`nome`) values (13,'MG','Minas Gerais');
insert into `estado`(`id_estado`,`uf`,`nome`) values (14,'PA','Par�');
insert into `estado`(`id_estado`,`uf`,`nome`) values (15,'PB','Para�ba');
insert into `estado`(`id_estado`,`uf`,`nome`) values (16,'PR','Paran�');
insert into `estado`(`id_estado`,`uf`,`nome`) values (17,'PE','Pernambuco');
insert into `estado`(`id_estado`,`uf`,`nome`) values (18,'PI','Piau�');
insert into `estado`(`id_estado`,`uf`,`nome`) values (19,'RJ','Rio de Janeiro');
insert into `estado`(`id_estado`,`uf`,`nome`) values (20,'RN','Rio Grande do Norte');
insert into `estado`(`id_estado`,`uf`,`nome`) values (21,'RS','Rio Grande do Sul');
insert into `estado`(`id_estado`,`uf`,`nome`) values (22,'RO','Rond�nia');
insert into `estado`(`id_estado`,`uf`,`nome`) values (23,'RR','Roraima');
insert into `estado`(`id_estado`,`uf`,`nome`) values (24,'SC','Santa Catarina');
insert into `estado`(`id_estado`,`uf`,`nome`) values (25,'SP','S�o Paulo');
insert into `estado`(`id_estado`,`uf`,`nome`) values (26,'SE','Sergipe');
insert into `estado`(`id_estado`,`uf`,`nome`) values (27,'TO','Tocantins');


CREATE TABLE `motivo_chamado` (
  `id_motivo_chamado` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ientificador do motivo de chamado',
  `motivo` varchar(50) NOT NULL COMMENT 'descricao do motivo do chamado ex: elogios, reclama�oes, etc.',
  PRIMARY KEY (`id_motivo_chamado`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


insert into `motivo_chamado`(`id_motivo_chamado`,`motivo`) values (1,'D�vidas');
insert into `motivo_chamado`(`id_motivo_chamado`,`motivo`) values (2,'Elogios');
insert into `motivo_chamado`(`id_motivo_chamado`,`motivo`) values (3,'Sugest�es');


CREATE TABLE `tipo_chamado` (
  `id_tipo_chamado` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador da tabela',
  `tipo_chamado` varchar(80) NOT NULL COMMENT 'descri��o do tipo de chamado',
  PRIMARY KEY (`id_tipo_chamado`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


insert into `tipo_chamado`(`id_tipo_chamado`,`tipo_chamado`) values (1,'Telefone');
insert into `tipo_chamado`(`id_tipo_chamado`,`tipo_chamado`) values (2,'Chat');
insert into `tipo_chamado`(`id_tipo_chamado`,`tipo_chamado`) values (3,'Email');

CREATE TABLE `chamado` (
  `id_chamado` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador do chamado',
  `descricao` longtext NOT NULL COMMENT 'detalhamento do chamado',
  `id_estado` int(11) NOT NULL,
  `id_tipo_chamado` int(11) NOT NULL,
  `id_motivo_chamado` int(11) NOT NULL,
  `data_encerramento` date DEFAULT NULL COMMENT 'data e hora do encerramento do chamado',
  PRIMARY KEY (`id_chamado`,`id_estado`,`id_tipo_chamado`,`id_motivo_chamado`),
  KEY `fk_chamado_estado_idx` (`id_estado`),
  KEY `fk_chamado_tipo_chamado1_idx` (`id_tipo_chamado`),
  KEY `fk_chamado_motivo_chamado1_idx` (`id_motivo_chamado`),
  CONSTRAINT `fk_chamado_estado` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id_estado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_chamado_motivo_chamado1` FOREIGN KEY (`id_motivo_chamado`) REFERENCES `motivo_chamado` (`id_motivo_chamado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_chamado_tipo_chamado1` FOREIGN KEY (`id_tipo_chamado`) REFERENCES `tipo_chamado` (`id_tipo_chamado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;


insert into `chamado`(`id_chamado`,`descricao`,`id_estado`,`id_tipo_chamado`,`id_motivo_chamado`,`data_encerramento`) values (1,'teste',2,1,2,'2016-08-05');
insert into `chamado`(`id_chamado`,`descricao`,`id_estado`,`id_tipo_chamado`,`id_motivo_chamado`,`data_encerramento`) values (2,'teste',9,1,2,'2016-08-05');
insert into `chamado`(`id_chamado`,`descricao`,`id_estado`,`id_tipo_chamado`,`id_motivo_chamado`,`data_encerramento`) values (3,'teste',2,1,2,'2016-08-05');
insert into `chamado`(`id_chamado`,`descricao`,`id_estado`,`id_tipo_chamado`,`id_motivo_chamado`,`data_encerramento`) values (4,'Bl�  Bl�, Bl�',7,3,2,'2016-08-05');
insert into `chamado`(`id_chamado`,`descricao`,`id_estado`,`id_tipo_chamado`,`id_motivo_chamado`,`data_encerramento`) values (5,'Duvida sobre qual a Capital do Amap�.',3,1,1,'2016-08-05');
insert into `chamado`(`id_chamado`,`descricao`,`id_estado`,`id_tipo_chamado`,`id_motivo_chamado`,`data_encerramento`) values (6,'D�vida sobre quem � o presidente do Brasil',7,2,2,'2016-08-05');
insert into `chamado`(`id_chamado`,`descricao`,`id_estado`,`id_tipo_chamado`,`id_motivo_chamado`,`data_encerramento`) values (7,'Quem � esse Pokemon?',6,2,2,'2016-08-04');
insert into `chamado`(`id_chamado`,`descricao`,`id_estado`,`id_tipo_chamado`,`id_motivo_chamado`,`data_encerramento`) values (8,'Onde est�o as Esferas do Drag�o?',4,1,3,'2016-08-04');
insert into `chamado`(`id_chamado`,`descricao`,`id_estado`,`id_tipo_chamado`,`id_motivo_chamado`,`data_encerramento`) values (9,'Quem ser� o Rei do Game Of Thrones',20,3,2,'2016-08-03');
insert into `chamado`(`id_chamado`,`descricao`,`id_estado`,`id_tipo_chamado`,`id_motivo_chamado`,`data_encerramento`) values (10,'Ser� que Jon Snow est� vivo?',3,1,1,'2016-08-06');
insert into `chamado`(`id_chamado`,`descricao`,`id_estado`,`id_tipo_chamado`,`id_motivo_chamado`,`data_encerramento`) values (11,'Tirar Fatima Bernardes do Ar',11,1,3,'2016-08-06');
insert into `chamado`(`id_chamado`,`descricao`,`id_estado`,`id_tipo_chamado`,`id_motivo_chamado`,`data_encerramento`) values (12,'Excelente Servi�o',5,2,2,'2016-08-06');
insert into `chamado`(`id_chamado`,`descricao`,`id_estado`,`id_tipo_chamado`,`id_motivo_chamado`,`data_encerramento`) values (13,'Quem Descobriu o Brasil?',2,3,1,'2016-08-06');
